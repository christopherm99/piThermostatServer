#define BUFSIZE                    128   // Size of the read buffer for incoming data
#define VERBOSE_MODE               true  // If set to 'true' enables debug output
#define BLUEFRUIT_HWSERIAL_NAME    Serial1
#define BLUEFRUIT_UART_MODE_PIN    12    // Set to -1 if unused
